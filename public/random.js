var numComics = 12; //Change to reflect the number of comics


//Get the page's URL to see what comic number it's on
var sep1 = document.location.toString().lastIndexOf("/") + 1;
var sep2 = document.location.toString().lastIndexOf(".htm");
var currentComic = document.location.toString().substring(sep1,sep2);

//Makes sure the Random button doesn't give you the same comic
while (true){
var cmcNumber = Math.floor(Math.random()*numComics)+1;
if (currentComic != cmcNumber)
break;
}

//Put the random button into the table
document.write('</td><td><a href = "' + cmcNumber + '.htm">Random</a></td>');